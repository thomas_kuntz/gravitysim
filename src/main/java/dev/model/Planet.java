package dev.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@Entity
@XmlRootElement
public class Planet implements Serializable{

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 1, max = 25)
    private String name;

    @NotNull
    private double diameter;

    @NotNull
    private double positionX;
    
    @NotNull
    private double positionY;
    
    @NotNull
    private double positionZ;
    
    @NotNull
    private double initialSpeedX;
    
    @NotNull
    private double initialSpeedY;
    
    @NotNull
    private double initialSpeedZ;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDiameter() {
		return diameter;
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}

	public double getPositionX() {
		return positionX;
	}

	public void setPositionX(double positionX) {
		this.positionX = positionX;
	}

	public double getPositionY() {
		return positionY;
	}

	public void setPositionY(double positionY) {
		this.positionY = positionY;
	}

	public double getPositionZ() {
		return positionZ;
	}

	public void setPositionZ(double positionZ) {
		this.positionZ = positionZ;
	}

	public double getInitialSpeedX() {
		return initialSpeedX;
	}

	public void setInitialSpeedX(double initialSpeedX) {
		this.initialSpeedX = initialSpeedX;
	}

	public double getInitialSpeedY() {
		return initialSpeedY;
	}

	public void setInitialSpeedY(double initialSpeedY) {
		this.initialSpeedY = initialSpeedY;
	}

	public double getInitialSpeedZ() {
		return initialSpeedZ;
	}

	public void setInitialSpeedZ(double initialSpeedZ) {
		this.initialSpeedZ = initialSpeedZ;
	}

	public Long getId() {
		return id;
	}
    
    
}
